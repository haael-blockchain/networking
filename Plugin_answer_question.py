import logging

from slixmpp.stanza import Message
from slixmpp.xmlstream import ElementBase, register_stanza_plugin
from slixmpp.plugins.base import BasePlugin

from slixmpp.xmlstream.handler import Callback
from slixmpp.xmlstream.matcher import StanzaPath

log = logging.getLogger(__name__)

class Plugin_answer_question(BasePlugin):
    def plugin_init(self):
        self.description = "Plugin, extending messages with answer-question tags."
        self.xep = "PAQ"
        dependencies = {'xep_0030'}
        self.is_extension = True

    def post_init(self):
        BasePlugin.post_init(self)
        self.xmpp.register_handler(
            Callback('message',
                     StanzaPath('send_message'),
                     self.extend_message))
        register_stanza_plugin(Message, Tags_extension)

    def extend_message(self, msg):
        msg = Tags_extension.handle_plugin(msg)
        
class Tags_extension(ElementBase):
    name = "plugin_answer_question"
    namespace = "https://haael.net/ns/blockchain"
    interfaces = {"question", "answer"}
    plugin_attrib = "paq"
    
    def handle_plugin(cls, msg):
        return msg
