#!/usr/bin/python3
#-*- coding:utf8 -*-


"""
BitTorrent - bridge between libtorrent and gobject.
"""


__author__ = "guaz, haael"
__copyright__ = "Copyright 2019, haael.co.uk/prim LTD"
__license__ = "GPLv3"
__credits__ = []


__all__ = 'BitTorrent',


import libtorrent
import sys
import time
import urllib.parse
import logging
try:
	import miniupnpc
except ImportError:
	logging.Logger(__name__).error("Module `miniupnpc` not found.")
import random
import pickle
from pathlib import Path
import shutil
import gi
from gi.repository import GObject as gobject
from threading import Thread, RLock


logging.basicConfig(level=logging.INFO)


def session_lock(old_method):
	def new_method(self, *args, **kwargs):
		with self.session_lock:
			return old_method(self, *args, **kwargs)
	
	new_method.__name__ = old_method.__name__
	return new_method


class BitTorrent(gobject.GObject):
	__gsignals__ = {
		'start': (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, ()),
		'heartbeat': (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, ()),
		'finished': (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, ()),
		'add_torrent_alert': (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT, gobject.TYPE_PYOBJECT)),
		'state_changed_alert': (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT, gobject.TYPE_INT, gobject.TYPE_INT)),
		'external_ip_alert': (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (gobject.TYPE_STRING,)),
		'torrent_added_alert': (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,)),
		'torrent_checked_alert': (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,)),
		'torrent_paused_alert': (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,)),
		'torrent_resumed_alert': (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,)),
		'metadata_received_alert': (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,)),
		'dht_outgoing_get_peers_alert': (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT, gobject.TYPE_PYOBJECT, gobject.TYPE_PYOBJECT)),
		'dht_bootstrap_alert': (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, ()),
		'dht_get_peers_alert': (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,)),
	}
		#'hash_failed_alert'
		#'dht_error_alert'
		#'torrent_finished_alert'
	
	dht_bootstrap_nodes = {('dht.libtorrent.org', 25401), ('router.utorrent.com', 6881), ('router.bittorrent.com', 6881), ('dht.transmissionbt.com', 6881), ('router.bitcomet.com', 6881), ('dht.aelitis.com', 6881)}
	root_directory = Path('supercomputer.network')
	magnet_format = 'magnet:?xt=urn:btih:{:s}'
	creator = "Supercomputer Network"
	upnp_description = "Blockchain BitTorrent service"
	alert_priority = 150
	
	def __init__(self, name=None, session_path=None, download_path=None, ports=(6881, 6881+10)):
		super().__init__()
		self.name = name or ('@' + hex(id(self))[2:])
		self.log = logging.getLogger(__name__ + '.' + self.__class__.__qualname__ + '.' + self.name)
		self.log.setLevel(logging.DEBUG)
		self.log.debug("BitTorrent object created.")
		self.download_path = (self.root_directory / 'data') if (download_path == None) else download_path
		self.session_path = (self.root_directory / 'session') if (session_path == None) else session_path
		self.dht_saved_nodes = set()
		self.dht_active_nodes = set()
		self.ports = ports
		self.session_lock = RLock()
	
	def __enter__(self):
		if not self.download_path.exists():
			self.download_path.mkdir(parents=True)
		
		if not self.session_path.exists():
			self.session_path.mkdir(parents=True)
		
		try:
			self.enable_port_redirection()
		except NameError:
			self.log.warning("Unable to setup UPnP port redirection.")
		
		self.start_session()
		
		try:
			self.load_dht_nodes()
		except FileNotFoundError as error:
			self.log.info("DHT nodes file not present: '%s'.", error.filename)
		except IOError as error:
			self.log.warning("DHT nodes not loaded: %s.", str(error))
		
		try:
			self.load_session()
		except FileNotFoundError as error:
			self.log.info("Session file not present: '%s'.", error.filename)
		except IOError as error:
			self.log.warning("Session not loaded: %s.", str(error))
		
		for torrent_file in self.session_path.iterdir():
			if torrent_file.suffix != '.torrent': continue
			try:
				self.resume_torrent(torrent_file)
			except RuntimeError as error:
				self.log.error("Could not resume torrent: %s.", str(error))
		
		for download_dir in self.download_path.iterdir():
			if (self.session_path / (download_dir.name + '.torrent')).exists(): continue # No need to seed dirs that have a torrent file, as they will be seeded from `load_session`.
			# TODO: add check if the link is in unfinished magnets
			magnet = self.seed(download_dir)
			encoded = self.encoded_name(magnet)
			if encoded != download_dir.name:
				shutil.rmtree(str(download_dir))
		
		try:
			self.load_magnets()
		except FileNotFoundError as error:
			self.log.info("File with magnet links without metadata not present: '%s'.", error.filename)
		except IOError as error:
			self.log.warning("Magnet links without metadata not restored: %s.", str(error))
		
		return self
	
	def __exit__(self, exc_type, exc_value, traceback):
		if exc_type:
			self.log.warn("Abnormal termination of BitTorrent session.")
		
		try:
			self.save_magnets()
		except IOError as error:
			self.log.error("Magnet links without metadata not saved: %s.", str(error))
		
		with self.session_lock:
			for torrent in self.session.get_torrents():
				try:
					self.store_torrent(torrent)
				except (IOError, RuntimeError) as error:
					self.log.error("Torrent fast resume data not saved: %s.", str(error))
		
		try:
			self.save_session()
		except IOError as error:
			self.log.warning("Session not saved: %s.", str(error))
		
		try:
			self.save_dht_nodes()
		except IOError as error:
			self.log.warning("DHT nodes not saved: %s.", str(error))
		
		self.stop_session()
		
		try:
			self.disable_port_redirection()
		except AttributeError:
			self.log.warning("Unable to undo UPnP port redirection.")
	
	@session_lock
	def start_session(self):
		self.log.info("Starting BitTorrent session.")
		self.session = libtorrent.session()
		
		alert_mask = 0
		alert_mask |= libtorrent.alert.category_t.status_notification
		alert_mask |= libtorrent.alert.category_t.dht_notification
		
		self.session.set_alert_mask(alert_mask)
		self.session.listen_on(*self.ports)
		self.log.debug("BitTorrent session started.")
	
	@session_lock
	def stop_session(self):
		self.log.info("Stopping BitTorrent session.")
		del self.session
		self.log.debug("BitTorrent session stopped.")
	
	@session_lock
	def load_session(self):
		self.log.info("Loading stored session state.")
		with (self.session_path / 'session.pickle').open('rb') as f:
			data = pickle.load(f)
		self.session.pause()
		self.session.load_state(data)
		self.session.resume()
		self.log.debug("Stored session state loaded.")
	
	@session_lock
	def save_session(self):
		self.log.info("Saving session state.")
		self.session.pause()
		data = self.session.save_state()
		self.session.resume()
		with (self.session_path / 'session.pickle').open('wb') as f:
			pickle.dump(data, f)
		self.log.debug("Session state saved.")
	
	def enable_port_redirection(self):
		self.log.info("Enabling UPnP port redirection.")
		self.upnp = miniupnpc.UPnP()
		self.upnp.discoverdelay = 500
		upnp_devices = self.upnp.discover()
		if not upnp_devices:
			self.log.info("No UPnP routers found.")
			return 0, 0
		
		self.upnp.selectigd()
		self.local_ip = self.upnp.lanaddr
		self.external_ip = self.upnp.externalipaddress()
		self.log.debug(" UPnP devices: %s", upnp_devices)
		self.log.debug(" local IP: %s", self.local_ip)
		self.log.debug(" external IP: %s", self.external_ip)
		
		successful_redirections = 0
		failed_redirections = 0
		for port in range(*self.ports):
			try:
				self.upnp.addportmapping(port, 'TCP', self.local_ip, port, self.upnp_description, '')
				self.log.debug(" Added mapping for TCP port %d.", port)
				successful_redirections += 1
			except:
				self.log.warn(" Unable to add mapping for TCP port %d.", port)
				failed_redirections += 1
			try:
				self.upnp.addportmapping(port, 'UDP', self.local_ip, port, self.upnp_description, '')
				self.log.debug(" Added mapping for UDP port %d.", port)
				successful_redirections += 1
			except:
				self.log.warn(" Unable to add mapping for UDP port %d.", port)
				failed_redirections += 1
		try:
			self.upnp.addportmapping(6771, 'UDP', self.local_ip, 6771, self.upnp_description, '')
			self.log.debug(" Added mapping for UDP port %d.", 6771)
			successful_redirections += 1
		except:
			self.log.warn(" Unable to add mapping for UDP port %d.", 6771)
			failed_redirections += 1
		self.log.debug("UPnP port redirection enabled.")
		return successful_redirections, failed_redirections
	
	def disable_port_redirection(self):
		self.log.info("Disabling UPnP port redirection.")
		for port in range(*self.ports):
			try:
				self.upnp.deleteportmapping(port, 'TCP', '')
				self.log.debug(" Removed mapping for TCP port %d.", port)
			except:
				pass
			try:
				self.upnp.deleteportmapping(port, 'UDP', '')
				self.log.debug(" Removed mapping for UDP port %d.", port)
			except:
				pass
		try:
			self.upnp.deleteportmapping(6771, 'UDP', '')
			self.log.debug(" Removed mapping for UDP port %d.", 6771)
		except:
			pass
		del self.upnp
	
	@session_lock
	def load_dht_nodes(self, sample=64):
		with (self.session_path / 'dht_nodes.pickle').open('rb') as f:
			self.dht_saved_nodes = frozenset(pickle.load(f))
		for dht_node in self.dht_bootstrap_nodes | frozenset(random.sample(self.dht_saved_nodes, k=min(sample, len(self.dht_saved_nodes)))):
			self.session.add_dht_node(dht_node)
	
	def save_dht_nodes(self, loaded=True, sample=256):
		nodes = self.dht_active_nodes | (self.dht_saved_nodes if loaded else frozenset())
		if sample < 0 or sample > len(nodes): sample = len(nodes)
		with (self.session_path / 'dht_nodes.pickle').open('wb') as f:
			pickle.dump(random.sample(nodes, k=sample), f)
	
	@staticmethod
	def encoded_name(magnet):
		#return urllib.parse.quote_plus(magnet)
		return magnet.split(':')[3]
	
	@session_lock
	def leech(self, magnet):
		self.log.info("Leeching torrent.")
		self.log.info(" %s", magnet)
		
		encoded = self.encoded_name(magnet)
		
		params = {
			'save_path':          str(self.download_path / encoded),
			'storage_mode':       libtorrent.storage_mode_t(2),
			'paused':             False,
			'auto_managed':       True,
			'duplicate_is_error': True
		}
		
		try:
			resume_file = self.session_path / (encoded + '.resume')
			with resume_file.open('rb') as f:
				params['resume_data'] = f.read()
			resume_file.unlink()
		except FileNotFoundError as error:
			self.log.info("Torrent fast resume data not present: '%s'.", error.filename)
		except IOError as error:
			self.log.warn("Error while fetching torrent fast resume data: %s.", str(error))
		
		self.log.debug("Adding torrent to session.")
		libtorrent.add_magnet_uri(self.session, magnet, params) # Will throw an exception if the torrent is already leeching.
		self.log.debug("Torrent leeching started.")
	
	@session_lock
	def seed(self, path):
		self.log.info("Seeding torrent.")
		self.log.debug(" Path: '%s'.", str(path))
		file_storage = libtorrent.file_storage()
		libtorrent.add_files(file_storage, str(path))
		
		t = libtorrent.create_torrent(file_storage)
		t.set_creator(self.creator)
		libtorrent.set_piece_hashes(t, '.')
		
		torrent = t.generate()
		torrent_info = libtorrent.torrent_info(torrent)
		magnet = self.magnet_format.format(str(torrent_info.info_hash()))
		encoded = self.encoded_name(magnet)
		
		if path != self.download_path / encoded:
			self.log.info("Copying torrent contents.")
			self.log.debug(" from='%s'", str(path))
			self.log.debug("   to='%s'", str(self.download_path / encoded))
			shutil.copy(path, self.download_path / encoded) # TODO
			
			file_storage = libtorrent.file_storage()
			libtorrent.add_files(file_storage, str(self.download_path / encoded))
			
			t = libtorrent.create_torrent(file_storage)
			t.set_creator(self.creator)
			libtorrent.set_piece_hashes(t, '.')
			
			torrent = t.generate()
			torrent_info = libtorrent.torrent_info(torrent)
		
		assert magnet == self.magnet_format.format(str(torrent_info.info_hash()))
		
		self.log.info(" %s", magnet)
		
		params = {
			'ti':                 torrent_info,
			'save_path':          str(self.download_path / encoded),
			'seed_mode':          True,
			'paused':             False,
			'auto_managed':       True,
			'duplicate_is_error': True
		}
		
		self.log.debug("Adding torrent to session.")
		self.session.add_torrent(params) # Will throw an exception if the torrent is already seeding.
		
		self.log.debug("Saving torrent metadata.")
		torrent_file = self.session_path / (encoded + '.torrent')
		with torrent_file.open('wb') as f:
			f.write(libtorrent.bencode(torrent))
		
		self.log.debug("Torrent seeding started.")
		return magnet
	
	@session_lock
	def resume_torrent(self, torrent_file):
		self.log.info("Resuming torrent.")
		torrent_info = libtorrent.torrent_info(str(torrent_file))
		magnet = self.magnet_format.format(str(torrent_info.info_hash()))
		self.log.info(" %s", magnet)
		encoded = self.encoded_name(magnet)
		
		params = {
			'ti':                 torrent_info,
			'save_path':          str(self.download_path / encoded),
			'seed_mode':          True,
			'paused':             False,
			'auto_managed':       True,
			'duplicate_is_error': True
		}
		
		try:
			resume_file = self.session_path / (encoded + '.resume')
			with resume_file.open('rb') as f:
				params['resume_data'] = f.read()
			resume_file.unlink()
		except IOError as error:
			self.log.warn("Error while fetching torrent fast resume data: %s.", str(error))
		
		self.log.debug("Adding torrent to session.")
		self.session.add_torrent(params)
		self.log.debug("Torrent resumed.")
	
	def store_torrent(self, torrent):
		torrent_info = libtorrent.torrent_info(torrent) # will raise RuntimeError if storing resume data is impossible
		magnet = self.magnet_format.format(str(torrent_info.info_hash()))
		encoded = self.encoded_name(magnet)
		resume_data = torrent.write_resume_data()
		data = libtorrent.bencode(resume_data)
		with (self.session_path / (encoded + '.resume')).open('wb') as f:
			f.write(data)
	
	@session_lock
	def save_magnets(self):
		magnets = set()
		for torrent in self.session.get_torrents():
			if torrent.has_metadata(): continue
			magnet = self.magnet_format.format(str(torrent.info_hash()))
			magnets.add(magnet)
		with (self.session_path / 'magnets.pickle').open('wb') as f:
			pickle.dump(magnets, f)
	
	def load_magnets(self):
		with (self.session_path / 'magnets.pickle').open('rb') as f:
			magnets = pickle.load(f)
		for magnet in magnets:
			self.leech(magnet)
	
	@staticmethod
	def object_to_dict(obj):
		d = dict()
		for attr in dir(obj):
			if attr[0] == '_': continue
			d[attr] = getattr(obj, attr)
		return d
	
	torrent_status_names = ['queued', 'checking', 'downloading metadata', 'downloading', 'finished', 'seeding', 'allocating', 'checking fastresume']
	
	peer_flag_names = ['interesting', 'choked', 'remote_interested', 'remote_choked', 'supports_extensions', 'local_connection', 'handshake', \
					   'connecting', 'on_parole', 'seed', 'optimistic_unchoke', 'snubbed', 'upload_only', 'endgame_mode', 'holepunched', \
					   'i2p_socket', 'utp_socket', 'ssl_socket', 'rc4_encrypted', 'plaintext_encrypted']
	
	@classmethod
	def parse_peer_flags(cls, flags):
		result = []
		for n, name in enumerate(cls.peer_flag_names):
			if flags & (1 << n):
				result.append(name)
		return result
	
	@session_lock
	def unfinished_torrents(self):
		unfinished = []
		for torrent in self.session.get_torrents():
			status = torrent.status()
			if status.state not in {4, 5}:
				unfinished.append(torrent)
		return unfinished
	
	@session_lock
	def process_alerts(self):
		for alert in self.session.pop_alerts():
			alert_type = str(alert.what())
			alert_message = str(alert.message())
			self.log.debug("Alert received: %s: '%s'.", alert_type, alert_message)
			
			if alert_type == 'state_changed_alert':
				def emit_signal(alert_type=alert_type, alert=alert):
					handle = alert.handle
					state = int(alert.state)
					prev_state = int(alert.prev_state)
					gobject.idle_add(lambda: self.emit(alert_type, handle, state, prev_state), priority=self.alert_priority)
					if not self.unfinished_torrents():
						gobject.idle_add(lambda: self.emit('finished'), priority=self.alert_priority+10)
				emit_signal()
			elif alert_type == 'external_ip_alert':
				def emit_signal(alert_type=alert_type, alert=alert):
					external_address = str(alert.external_address)
					gobject.idle_add(lambda: self.emit(alert_type, external_address), priority=self.alert_priority)
				emit_signal()
			elif alert_type == 'add_torrent_alert':
				def emit_signal(alert_type=alert_type, alert=alert):
					params = alert.params
					error_code = alert.error
					gobject.idle_add(lambda: self.emit(alert_type, params, error_code), priority=self.alert_priority)
				emit_signal()
			elif alert_type in ['torrent_added_alert', 'torrent_removed_alert', 'torrent_paused_alert', 'torrent_resumed_alert', 'torrent_checked_alert']:
				def emit_signal(alert_type=alert_type, alert=alert):
					handle = alert.handle
					gobject.idle_add(lambda: self.emit(alert_type, handle), priority=self.alert_priority)
				emit_signal()
			elif alert_type == 'metadata_received_alert':
				def emit_signal(alert_type=alert_type, alert=alert):
					handle = alert.handle
					gobject.idle_add(lambda: self.handle_metadata_received(handle), priority=self.alert_priority-10)
					gobject.idle_add(lambda: self.emit(alert_type, handle), priority=self.alert_priority)
				emit_signal()
			elif alert_type == 'dht_outgoing_get_peers_alert':
				def emit_signal(alert_type=alert_type, alert=alert):
					#print(dir(alert))
					info_hash = alert.info_hash
					obfuscated_info_hash = alert.obfuscated_info_hash
					endpoint = tuple(alert.ip)
					gobject.idle_add(lambda: self.handle_dht_outgoing_get_peers_alert(info_hash, obfuscated_info_hash, endpoint), priority=self.alert_priority-10)
					gobject.idle_add(lambda: self.emit(alert_type, info_hash, obfuscated_info_hash, endpoint), priority=self.alert_priority)
				emit_signal()
			elif alert_type == 'dht_bootstrap_alert':
				def emit_signal(alert_type=alert_type, alert=alert):
					#print(self.session.dht_state().nodes)
					gobject.idle_add(lambda: self.emit(alert_type), priority=self.alert_priority)
				emit_signal()
			elif alert_type == 'dht_get_peers_alert':
				def emit_signal(alert_type=alert_type, alert=alert):
					info_hash = alert.info_hash
					gobject.idle_add(lambda: self.emit(alert_type, info_hash), priority=self.alert_priority)
				emit_signal()
			else:
				self.log.warning("Unsupported alert: %s", alert_type)
	
	def handle_metadata_received(self, torrent):
		assert torrent.has_metadata()
		magnet = self.magnet_format.format(str(torrent.info_hash()))
		encoded = self.encoded_name(magnet)
		torrent_file = self.session_path / (encoded + ".torrent")
		if not torrent_file.exists():
			with torrent_file.open('wb') as f:
				torrent_data = torrent.get_torrent_info()
				f.write(libtorrent.bencode(libtorrent.create_torrent(torrent_data).generate()))
				#f.write(libtorrent.bencode(torrent))
	
	def handle_dht_outgoing_get_peers_alert(self, info_hash, obfuscated_info_hash, endpoint):
		#self.log.debug("DHT get peers %s, %s:%d", info_hash, endpoint[0], endpoint[1])
		self.dht_active_nodes.add(endpoint)
	
	def schedule_background_thread(self, mainloop, polltimeout=1000):
		bittorrent = self
		class LibTorrentThread(Thread):
			def run(self):
				with bittorrent:
					assert hasattr(bittorrent, 'session')
					gobject.idle_add(lambda: bittorrent.emit('start'), priority=bittorrent.alert_priority+10)
					while mainloop.is_running():
						bittorrent.session.wait_for_alert(polltimeout)
						bittorrent.process_alerts()
						gobject.idle_add(lambda: bittorrent.emit('heartbeat'), priority=bittorrent.alert_priority+10)
		
		libtorrent_thread = LibTorrentThread()
		gobject.idle_add(lambda: bool(libtorrent_thread.start() and False), priority=bittorrent.alert_priority+20)
	
	def handler(self, handler):
		self.log.debug("Registered handler for signal '%s'.", handler.__name__)
		self.connect(handler.__name__, lambda _self, *_args: handler(*_args))
		return handler

gobject.type_register(BitTorrent)


if __debug__ and __name__ == "__main__":
	magnets_to_download = ['magnet:?xt=urn:btih:1dde0e926fead0f64b225790bc97799641e4e0bf&dn=Toaru Kagaku no Railgun T - 01 (480p-HorribleSubs[TGx]&tr=udp://tracker.coppersurfer.tk:6969&tr=udp://tracker.opentrackr.org:1337&tr=udp://tracker.pirateparty.gr:6969&tr=udp://9.rarbg.to:2710&tr=udp://9.rarbg.me:2710']
	
	gobject.threads_init() # init gobject thread structures
	mainloop = gobject.MainLoop() # create main loop
	
	BitTorrent.root_directory = Path('test_bittorrent') # set working directory
	bittorrent = BitTorrent() # create BitTorrent object
	
	@bittorrent.handler # register start handler
	def start():
		for url in magnets_to_download:
			try:
				bittorrent.leech(url) # leech magnet url to download
			except RuntimeError as error:
				logging.getLogger(__name__).warning("Could not leech torrent: %s", error)
	
	@bittorrent.handler # register handler when all torrents have been downloaded
	def finished():
		mainloop.quit() # quit the main loop
	
	bittorrent.schedule_background_thread(mainloop) # spawn LibTorrent thread in background
	
	try:
		mainloop.run() # run the main loop
	except KeyboardInterrupt:
		print()


