import logging

from slixmpp.stanza import Message

from slixmpp.xmlstream import ElementBase, ET, JID, register_stanza_plugin
from slixmpp import Iq

from slixmpp.plugins.base import BasePlugin

from slixmpp.xmlstream.handler import Callback
from slixmpp.xmlstream.matcher import StanzaPath
from slixmpp.xmlstream.matcher import MatchXPath

from DNS_resolver import DNS_resolver

log = logging.getLogger(__name__)

class ext_DNS(BasePlugin):
    def plugin_init(self):
        self.description = "DNS plugin"                             ##~ Description if requested
        self.xep = "DNS"                                            ##~ Identifier if requested
        #~ self.is_extension = True                                    ##~ Information if this plugin extend something, default False
        
        BasePlugin.post_init(self)                                  ##~ Initialize base plugin
        self.xmpp.register_handler(
            Callback('request',
                     StanzaPath('iq/{{{}}}dns'.format(DNSPlugin.namespace)),
                     self.__handleDNS))                             ##~ Register handler for dns query

        register_stanza_plugin(Iq, DNSPlugin)                       ##~ Register tags extension for Iq object, otherwise iq['dns'] will be string field instead container.

    #~ def post_init(self):
        #~ BasePlugin.post_init(self)
        #~ self.xmpp['xep_0030'].add_feature(DNSPlugin.namespace)
        #~ self.xmpp.register_handler( Callback("DNS plugin",
                                        #~ MatchXPath('{0:s}iq/{{{1:s}}}dns'.format(self.xmpp.default_ns, DNSPlugin.namespace)),
                                        #~ self.__handleDNS))
        #~ self.xmpp.register_handler( Callback("DNS plugin",
                                        #~ MatchXPath('{0:s}iq/{{jabber:iq:dns}}dns'.format(self.xmpp.default_ns)),
                                        #~ self.__handleDNS))

        #~ self.xmpp.register_handler(
            #~ Callback('request',
                     #~ StanzaPath('message/eme'),
                     #~ self._handle_request))
        #~ register_stanza_plugin(Message, DNSPlugin)


    def __handleDNS(self, iq):
        if iq['dns'] and iq['dns']['name'] and iq['dns']['res']:
            if iq['type'] == 'get':
                iq_result = self.build_DNS_result(iq)               ##~ Get builded xml result with dig respond 
                self.xmpp.event('iq_dns_answer', iq_result)         ##~ Send final build to handler
            if iq['type'] == 'result':
                self.xmpp.event('iq_dns_receive', iq)
                

    def build_DNS_result(self, dns_iq):
        dns_resolver = DNS_resolver(["8.8.8.8", "8.8.4.4"], True)   ##~ Build class for dig request
        dns_name = dns_iq['dns']['name']                            ##~ Get name from dns request
        dns_res = dns_iq['dns']['res']                              ##~ Get res from dns request
        dns_results = dns_resolver.request(dns_name, dns_res)       ##~ Process dns request with dig
        iq_result = self.xmpp.make_iq(itype='result')               ##~ Make new iq stanza to result
        iq_result['to'], iq_result['from'] = dns_iq['from'], dns_iq['to'] ##~ Put reversed from/to inside iq
        iq_result['dns'].fillInterfaces(dns_name, dns_res)          ##~ Fill name and res inside iq
        iq_result['dns'].addField(dns_results)                      ##~ Add tags inside dns from results
        return iq_result
                
class DNSPlugin(ElementBase):
    name = "dns"                                                    ##~ The name of the root XML element.
    namespace = "https://haael.net/ns/blockchain"                   ##~ The namespace our stanza object lives in like <dns xmlns={namespace} (...)</dns>

    plugin_attrib = "dns"                                           ##~ The name to access this type of stanza. In particular, given  a  registration  stanza,  the Registration object can be found using: dns_object['dns']
    
    interfaces = {"name", "res"}                                    ##~ A list of dictionary-like keys that can be used with the stanza object. For example `dns_object['dns']`
    #~ sub_interfaces = {"name", "res"}                                ##~ A subset of interfaces, but these keys map to the text of any subelements that are direct children of the main stanza element. For example `dns_object['dns']['name']`

    def fillInterfaces(self, name, res):
        """add or update tags in dns tag
        :arg str name: url name for dig checking
        :arg str res: res type for dig checking"""
        self.xml.attrib.update({'name': name})                      ##~ Add/update name parameter
        self.xml.attrib.update({'res': res})                        ##~ Add/update res parameter

    def addField(self, dig_respond):
        """add xml tags into dns tag, from list of dictionaries
        :arg list dig_respond: list of dictionaries || Required """
        for tag in dig_respond:
            rrtype = tag.pop("rrtype", None)
            signature = tag.pop("signature", None)
            addr = tag.pop("addr", None)
            if rrtype:
                itemXML = ET.Element("{{{0:s}}}{1:s}".format(self.namespace, rrtype))
                if addr:
                    itemXML.text = addr
                elif signature:
                    itemXML.text = signature
                itemXML.attrib.update(tag)
                self.xml.append(itemXML)
