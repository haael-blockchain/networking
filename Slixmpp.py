#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
from getpass import getpass
from argparse import ArgumentParser
import slixmpp
import time

import Plugin_answer_question
import ext_DNS

import dns.resolver as dns_res
import dns.message as dns_msg
import dns.rdatatype

class EchoBot(slixmpp.ClientXMPP):
    def __init__(self, jid, password):
        slixmpp.ClientXMPP.__init__(self, jid, password)        #~ Parent init

        self.add_event_handler("session_start", self.start)     #~ Start session handler
        self.add_event_handler("message", self.message)         #~ Receive message handler

    def start(self, event):
        self.send_presence()                                    #~ Send presence to be active
        try:
            self.get_roster()                                   #~ Save clients JID's
        except IQTimeout:
            print("Your connection is excessively slow or server is no longer responding.")
        
        #~ self.client_roster.subscribe("jid")      #~ General case for reverse blocking after reboot 
        #~ self.update_roster_item("jid")           #~ General case for reverse blocking after reboot

    def message(self, msg):
        show_status = False
        sender_jid = str(msg['from']).split("/")[0] #~ JID without software used
        message_text = str(msg['body'])             #~ Body of plain text
            
        if __debug__: #~ Looking for msg xml tags.
            print(msg)

        ###~ Bot options ~###
        if message_text.startswith("b!"):   #~ Temporary, test prefix for bot commands
            show_status = True
            message_text = message_text[2:]
            if message_text.startswith("block"):
                self.update_roster(sender_jid, groups=["Blocked"])
                #~ self.client_roster.unsubscribe(sender_jid) #~ Delete from friends 
                #~ self.del_roster_item(sender_jid) #~ End channel and block messages from sender
            elif message_text.startswith("unknown"):
                self.update_roster(sender_jid, groups=["Stranger"])
                #~ self.del_roster_item(sender_jid) #~ End channel and block messages from sender
            elif message_text.startswith("known"):
                self.update_roster(sender_jid, groups=["Friend"])
                #~ self.client_roster.subscribe(sender_jid) #~ Sending invite if actually isn't added.
            elif message_text.startswith("trusted"):
                self.update_roster(sender_jid, groups=["Trusted"])
                #~ self.client_roster.subscribe(sender_jid) #~ Sending invite if actually isn't added.
                
        ###~ DNS request/response ~###
        if msg["dns"]:
            self.handle_dns(msg)
                
        ###~ Plugin options ~###
        if msg['paq']['question']: #~ Handle action for questions
            self.handle_question(msg)
        if msg['paq']['answer']: #~ Handle action for answers
            self.handle_answer(msg)
                
        if msg['type'] in ('chat', 'normal'): #~ Filter only normal and chat messages
            if show_status:
                self.send_message_( msg_to=msg['from'],
                                    msg_body="Your status, looks like:\n{}".format(self.client_roster._jids.get(sender_jid)),
                                    msg_type=msg['type'],
                                    answer="Your status, looks like:\n{}".format(self.client_roster._jids.get(sender_jid)))
            else:
                self.send_message_( msg_to=msg['from'],
                                    msg_body="Thanks for sending\n{}".format(msg['body']),
                                    msg_type=msg['type'],
                                    question="Request is valid?")

    def send_message_(self, **kwargs):
        """Build message for Plugin test. There are two possible builder cases:
        -First priority case:
        :param str message: Message xmpp object || Required
        :param str question: Question tag info || Default None
        :param str answer: Answer tag info || Default None
        
        -Secound case:
        :param str msg_to: Target jabber ID for message || Required
        :param str msg_body: Message content || Required
        :param str msg_mtype: Type of message || Default 'chat'
        :param str question: Question tag info || Default None
        :param str answer: Answer tag info || Default None
        """
        msg_to = kwargs.pop("msg_to", None)
        msg_body = kwargs.pop("msg_body", None)
        msg_type = kwargs.pop("msg_type", 'chat')
        
        msg = kwargs.pop("msg", None)
        
        question = kwargs.pop("question", None)
        answer = kwargs.pop("answer", None)
        
        if __debug__: #~ Temporary assert
            assert not kwargs, "kwargs` should be empty after argparse. Some of parameters isn't recognized. Check doc-string."
        
        if (msg is None) and not (msg_to is None) and not (msg_body is None):
            msg = self.make_message(mto=msg_to, mbody=msg_body, mtype=msg_type)
        if msg:
            if not (question is None): msg["paq"]["question"] = question
            if not (answer is None): msg["paq"]["answer"] = answer
            msg.send()
            
    def send_dns_message(self, **kwargs):
        """Build message for Plugin test. There are two possible builder cases:
        -First priority case:
        :param str msg: Message xmpp object || Required
        :param str name: dns dig 'name' param || Default None
        :param str addr: dns dig 'addr' param || Default None
        :param str res:  dns dig 'res'  param || Default None
        :param str prio:  dns dig 'priority'  param || Default None
        
        -Secound case:
        :param str msg_to: Target jabber ID for message || Required
        :param str msg_body: Message content || Required
        :param str msg_type: Type of message || Default "chat"
        
        -DNS build (Three first required):
        :param str name: dns dig 'name' param || Required None
        :param str addr: dns dig 'addr' param || Required None
        :param str res:  dns dig 'res'  param || Required None
        :param str text: communique between <dns> tag || Required None
        :param str prio:  dns dig 'priority'  param || Default None
        """
        msg_to = kwargs.pop("msg_to", None)
        msg_body = kwargs.pop("msg_body", None)
        msg_type = kwargs.pop("msg_type", "chat")
        
        msg = kwargs.pop("msg", None)
        
        dns_name = kwargs.pop("name", None)
        dns_addr = kwargs.pop("addr", None)
        dns_res = kwargs.pop("res", None)
        dns_text = kwargs.pop("text", None)
        dns_priority = kwargs.pop("prio", None)
        
        if __debug__: #~ Temporary assert
            assert not kwargs, "`kwargs` should be empty after argparse. Some of parameters isn't recognized. Check doc-string."
        
        if (msg is None) and \
           not (msg_to is None) and \
           not (msg_body is None):
            msg = self.make_message(mto=msg_to, mbody=msg_body, mtype=msg_type)
        if not (dns_name is None) and \
           not (dns_addr is None) and \
           not (dns_res is None):
            if not (dns_text is None):
                msg["dns"].xml.text = dns_text
            msg["dns"]["name"] = dns_name
            msg["dns"]["addr"] = dns_addr
            msg["dns"]["res"] = dns_res
            msg["dns"]["priority"] = dns_priority
        if msg:
            msg.send()
            
    def handle_question(self, msg):
        msg_from = msg['from']
        msg_body = msg['body']
        msg_question = msg['question']
        #~ ToDo actions for handled question
        
    def handle_answer(self, msg):
        msg_from = msg['from']
        msg_body = msg['body']
        msg_question = msg['answer']
        #~ ToDo actions for handled question
        
    def handle_dns(self, dns_msg):
        if dns_msg["dns"].xml.text == "request": #~ If that message is request
            myResolver = dns_res.Resolver() 
            myResolver.nameservers = ['8.8.8.8', '8.8.4.4']
            myAnswers = myResolver.query(dns_msg["dns"]["name"], dns_msg["dns"]["res"]) 
            for rdata in myAnswers: #~ Extract IP
                ip = str(rdata)
            for rrset in myAnswers.response.answer: #~ Extract priority
                priority = str(rrset.ttl)
            rdname = str(myAnswers.qname) #~ Extract name of serwer
            self.send_dns_message(  msg_to=dns_msg["from"], msg_body="dns respond", 
                                    text="respond", name=rdname, prio=priority, addr=ip, res=str(dns_msg["dns"]["res"]))
        pass
        #~ ToDo actions for handled dns

if __name__ == '__main__':
    #~ Temporary commandline echobot arguments from docs.
    parser = ArgumentParser(description=EchoBot.__doc__)
    parser.add_argument("-q", "--quiet", help="set logging to ERROR",
                        action="store_const", dest="loglevel",
                        const=logging.ERROR, default=logging.INFO)
    parser.add_argument("-d", "--debug", help="set logging to DEBUG",
                        action="store_const", dest="loglevel",
                        const=logging.DEBUG, default=logging.INFO)
    parser.add_argument("-j", "--jid", dest="jid",
                        help="JID to use")
    parser.add_argument("-p", "--password", dest="password",
                        help="password to use")
    args = parser.parse_args()

    #~ logging for debuging, and check what is in transmission of bot.
    logging.basicConfig(level=args.loglevel, format='%(levelname)-8s %(message)s')

    if __debug__:
        if args.jid is None:
            args.jid = "test-slixmpp-bot@xmpp.jp"
        if args.password is None:
            args.password = "test-haslo"
    else:
        if args.jid is None:
            args.jid = input("Username: ")
        if args.password is None:
            args.password = getpass("Password: ")
    xmpp = EchoBot(args.jid, args.password)
    xmpp.register_plugin('xep_0030') #~ Dependency required for Plugin_test
    xmpp.register_plugin('xep_0199') # XMPP Ping
    xmpp.register_plugin('Plugin_answer_question', module=Plugin_answer_question)
    xmpp.register_plugin('ext_DNS', module=ext_DNS)

    # Connect to the XMPP server and start processing XMPP stanzas.
    xmpp.connect()
    xmpp.process()
