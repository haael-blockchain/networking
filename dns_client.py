#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    Slixmpp: The Slick XMPP Library
    Copyright (C) 2010  Nathanael C. Fritz
    This file is part of Slixmpp.

    See the file LICENSE for copying permission.
"""

import logging
from getpass import getpass
from argparse import ArgumentParser
import time

import slixmpp
import xml.etree.ElementTree as ET

import ext_DNS

class DNS_client(slixmpp.ClientXMPP):
    def __init__(self, args):
        slixmpp.ClientXMPP.__init__(self, args.jid, args.password)

        self.recipient = args.to
        self.url = args.url
        self.res = args.res
        self.body_msg = args.message

        self.add_event_handler("session_start", self.start)              #~ Post init method for session_start handler
        #~ self.add_event_handler("message", self.message)                  #~ Receive message handler if necessary

        self.add_event_handler("iq_dns_answer", self.dns_answer)
        self.add_event_handler("iq_dns_receive", self.dns_receive)

    def start(self, event):
        """
        Process the session_start event.

        Typical actions for the session_start event are
        requesting the roster and broadcasting an initial
        presence stanza.

        Arguments:
            event -- An empty dictionary. The session_start
                     event does not provide any additional
                     data.
        """
        #~ self.send_presence()                                         ##~ Send our presence to users
        #~ self.get_roster()                                            ##~ Send roster to server for roster result
        
        if __debug__:
            self.send_dns_request(  dns_to = self.recipient,
                                    dns_name = self.url,
                                    dns_res = self.res)

    def dns_answer(self, iq):
        iq.send()

    def dns_receive(self, iq):
        print("DNS Iq result:\n", iq)

    def send_dns_request(self, **kwargs):
        """Build and send dns iq get to send it
        :param str dns_to: full user jid || Required 
        :param str dns_name: dns dig 'name' param || Required
        :param str dns_res:  dns dig 'res'  param || Default "A"
        """
        dns_to = kwargs.pop("dns_to", None)
        dns_name = kwargs.pop("dns_name", None)
        dns_res = kwargs.pop("dns_res", "A")
        
        if __debug__: #~ Temporary assert
            assert not kwargs, "`kwargs` should be empty after argparse. Some of parameters isn't recognized. Check doc-string."
            assert dns_to, "U must give to who you want send this iq"
            assert dns_name, "U must give url name which should be checked"

        if not (dns_to is None) and not (dns_name is None) and not (dns_res is None):
            iq = self.make_iq(ito=dns_to, itype="get")
            iq["dns"].fillInterfaces(dns_name, dns_res)
            assert iq, "Iq wasn't created succesfully."
            iq.send()

if __name__ == '__main__':
    # Setup the command line arguments.
    parser = ArgumentParser(description=DNS_client.__doc__)

    # Output verbosity options.
    parser.add_argument("-q", "--quiet", help="set logging to ERROR",
                        action="store_const", dest="loglevel",
                        const=logging.ERROR, default=logging.INFO)
    parser.add_argument("-d", "--debug", help="set logging to DEBUG",
                        action="store_const", dest="loglevel",
                        const=logging.DEBUG, default=logging.INFO)

    # JID, password and another sending options.
    parser.add_argument("-j", "--jid", dest="jid",
                        help="JID to use")
    parser.add_argument("-p", "--password", dest="password",
                        help="password to use")
    parser.add_argument("-t", "--to", dest="to",
                        help="full JID to send the iq to")
    parser.add_argument("-u", "--url", dest="url",
                        help="url name to check")
    parser.add_argument("-r", "--res", dest="res",
                        help="res type to check")
    parser.add_argument("-m", "--message", dest="message",
                        help="message to send")

    args = parser.parse_args()

    # Setup logging.
    logging.basicConfig(level=args.loglevel,
                        format='%(levelname)-8s %(message)s')
                        
    logging.basicConfig(level=logging.ERROR,format='%(levelname)-8sessage)s')

    if args.jid is None:
        args.jid = input("Username: ")
    if args.password is None:
        args.password = getpass("Password: ")
    if args.to is None:
        args.to = input("Send To: ")
    if args.url is None:
        args.url = input("URL name: ")
    if args.res is None:
        args.res = input("RES type: ")
    if args.message is None:
        args.message = input("Message text: ")

    try:
        xmpp = DNS_client(args)                             ##~ Build own xmpp client extended class
        xmpp.register_plugin('ext_DNS', module=ext_DNS)     ##~ Register our own plugin

        # Connect to the XMPP server and start processing XMPP stanzas.
        xmpp.connect()                                      ##~ Must be, otherwise never connecting.
        xmpp.process()                                      ##~ Must be, otherwise: sys:1: RuntimeWarning: coroutine 'XMLStream._connect_routine' was never awaited

    except KeyboardInterrupt:
        xmpp.disconnect()
        exit(0)
