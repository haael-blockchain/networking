import logging

from slixmpp.stanza import Message #~ ToDo
from slixmpp.xmlstream import ElementBase
from slixmpp.plugins.base import BasePlugin

from slixmpp.xmlstream.handler import Callback
from slixmpp.xmlstream.matcher import StanzaPath

log = logging.getLogger(__name__)

class Plugin_test(BasePlugin):
    def plugin_init(self):
        self.description = "Test plugin, answer-question"
        self.xep = "Plugin_test"
        dependencies = {'xep_0030'}
        if __debug__:
            print("REGISTRED", self.description)

    def post_init(self):
        BasePlugin.post_init(self)
        self.xmpp.register_handler(
            Callback('question',
                     StanzaPath('message/eme'),
                     self._handle_question))
        self.xmpp.register_handler(
            Callback('answer',
                     StanzaPath('message/eme'),
                     self._handle_answer))

    def _handle_question(self, msg):
        self.xmpp.event('message_question', msg)
        
    def _handle_answer(self, msg):
        self.xmpp.event('message_answer', msg)

class Tags_test(ElementBase):
    name = "tags_test"
    namespace = "jabber:iq:test"
    plugin_attrib = {"question", "answer"}
    interfaces = {"namespace", "name"}

