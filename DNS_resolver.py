import subprocess

class DNS_resolver():
    def __init__(self, nameservers=[], dnssec=False):
        self.nameservers = nameservers
        self.dnssec = dnssec


    @staticmethod
    def __catch_answer_data(lines):
        "Parse lines into dict name:value to create ElementTree"
        results = []
        if not lines:
            return []
        for line in lines:
            splitted = line.split(maxsplit=12)
            data = {}
            if "RRSIG" in splitted:
                data["name"]            = splitted[0]
                data["priority"]        = splitted[1]
                data["rrtype"]          = splitted[3]
                data["alghoritm"]       = splitted[5]
                data["num_of_labels"]   = splitted[6]
                data["RRset_TTL"]       = splitted[7]
                data["expiration_time"] = splitted[8]
                data["inception_time"]  = splitted[9]
                data["key_tag"]         = splitted[10]
                data["signature_name"]  = splitted[11]
                data["signature"]       = splitted[12]
                results.append(data)
            else:
                data["name"]            = splitted[0]
                data["priority"]        = splitted[1]
                data["rrtype"]          = splitted[3]
                data["addr"]            = splitted[4]
                results.append(data)
        return results


    @staticmethod
    def __catch_answers(text_lines):
        "Parse output to get only `;; ANSWER SECTION:`"
        start = False
        begin, end = 0, 0
        for index, text_line in enumerate(text_lines, 1):
            if text_line.startswith(";; ANSWER"):
                start = True
                begin = index
                end = index
            elif start:
                if not text_line:
                    return text_lines[begin:end]
                else:
                    end += 1


    def request(self, domain, *args):
        "Send dns request to domain, this request would return dictionary data to create ElementTree with DNS tag"
        command_arguments = ["dig"]

        ##~ Append to command builder nameservers
        for nameserver in self.nameservers:
            if nameserver.count(".")==3:
                if nameserver.startswith("@"):
                    command_arguments.append(nameserver)
                else:
                    command_arguments.append("@"+nameserver)

        ##~ Append dnssec if required
        if self.dnssec:
            command_arguments.append("+dnssec")

        ##~ Append args parameters
        if "." in domain:
            command_arguments.append(domain)
        else:
            raise Exception #~ Placeholder for error: Domain should have dot in adres
        command_arguments += list(args)
        
        ##~ Join command, run dig and catch output.
        try:
            output = subprocess.check_output(command_arguments, 
                                            shell=False, universal_newlines=True)
        except subprocess.CalledProcessError: #~ Error with wrong arguments.
            return None

        ##~ Filter 
        lines = output.split("\n")
        answers = self.__catch_answers(lines)
        return self.__catch_answer_data(answers)


if __name__ == "__main__":
    dns = DNS_resolver(nameservers=["@8.8.8.8", "@8.8.4.4"], dnssec=True)
    print(dns.request("pir.org", "NS"))
    print(dns.request("pir.org", "A"))
    print(dns.request("google.com", "A"))
    print(dns.request("dnssec-failed.org", "AAAA"))
